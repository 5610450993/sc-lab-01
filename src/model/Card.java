package model;

public class Card {
	private int balance;
	private String name;

	public Card(int amount) {
		this.balance = amount;
	}

	public void deposit(int amount) {
		balance += amount;
	}
	
	public void withdraw(int amount) {
		balance -= amount;
	}
	
	public void setName(String str) {
		name = str;
	}

	public String toString() {
		return "  " + balance;
	}

}
