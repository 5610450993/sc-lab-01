package test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import model.Card;
import gui.SoftwareFrame;

public class SoftwareTest {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new SoftwareTest();
	}

	public SoftwareTest() {
		frame = new SoftwareFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		/*
		 * Student may modify this code and write your result here
		 * frame.setResult("aaa\t bbb\t ccc\t"); frame.extendResult("ddd");
		 */
		Card c = new Card(0);
		frame.setResult("Balance : " + c.toString());
		int amount = 200;
		frame.extendResult("Deposit : " + amount);
		c.deposit(amount);
		frame.extendResult("Balance : " + c.toString());
		amount = 50;
		frame.extendResult("Withdraw : " + amount);
		c.withdraw(amount);
		frame.extendResult("Balance : " + c.toString());

	}

	ActionListener list;
	SoftwareFrame frame;
}
